#!/bin/bash
apt-get update
apt-get install -y wget software-properties-common apt-transport-https ca-certificates curl gnupg-agent

if ! command -v python3 &>/dev/null; then
    echo "[INFO] Python not found. Installing Python 3.7 or newer..."
    apt-get update
    apt-get install -y python3
fi

# Python version is too old. Installing Python 3.7 or newer...
python_version=$(python3 -c "import sys; print(sys.version_info.major)")
if [[ $python_version -lt 3 ]]; then
    echo "[INFO] Python version is too old. Installing Python 3.7 or newer..."
    apt-get update
    apt-add-repository ppa:deadsnakes/ppa
    apt-get install -y python3.7
fi
echo "[INFO] Python is installed."

# install pip module from bootstrap
if ! command -v pip3 &>/dev/null; then
    echo "[INFO] Pip not found. Installing pip..."
    wget https://bootstrap.pypa.io/get-pip.py
    python3 get-pip.py
fi

# install docker.io
if ! command -v docker &>/dev/null; then
    echo "[INFO] Docker not found. Installing Docker..."
    apt-get update
    apt-get install -y docker.io
    systemctl start docker
    systemctl enable docker
fi

ID_RSA="-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABFwAAAAdzc2gtcn
NhAAAAAwEAAQAAAQEAuAxQy+l0nNAe6ViN1QYIcjEeGRteCay+TLWg1y+mf3C9KS3lBsUL
aNvdT1fQWHIQO4JF6OuCDgWPUGDPUNa72psDSzYj/dFkbD8N4rQUbVDecNKLejrjPlcovh
vKCfwOSCajlkCo9yrzQ3RAFFt4Yve2NuyZq22J9Hx15A56HfC+sxiVjVRsC53P23IEVnUt
GzqvpDI74hdxvbsLobwjRLfGJtgPu+fCaL7DYj244Uhh4/2ZshhhnyMkyLvj4/suWqZZYP
UQbHsg1occ7lvH6Ar6egCaYej4fZcFbrRIWXZSUjfxrpXmZY6dWgU2IdNee1HOZ+rgtnRh
oziAgBpFnwAAA9DNTfAYzU3wGAAAAAdzc2gtcnNhAAABAQC4DFDL6XSc0B7pWI3VBghyMR
4ZG14JrL5MtaDXL6Z/cL0pLeUGxQto291PV9BYchA7gkXo64IOBY9QYM9Q1rvamwNLNiP9
0WRsPw3itBRtUN5w0ot6OuM+Vyi+G8oJ/A5IJqOWQKj3KvNDdEAUW3hi97Y27JmrbYn0fH
XkDnod8L6zGJWNVGwLnc/bcgRWdS0bOq+kMjviF3G9uwuhvCNEt8Ym2A+758JovsNiPbjh
SGHj/ZmyGGGfIyTIu+Pj+y5apllg9RBseyDWhxzuW8foCvp6AJph6Ph9lwVutEhZdlJSN/
GuleZljp1aBTYh0157Uc5n6uC2dGGjOICAGkWfAAAAAwEAAQAAAQAJyonv1ysffojudAoT
l58SaB+ISWbuz9RN42m7mOrcI8TEcI8bFZd+TBgRdQP9+I95gvwyIHlKQqx1o7jTvaC/x/
hcY3R1t9KGWbJk6ZyelitHSEQoTAKz2dIuju7oKhMeboPMZgM3Jv9LCt+v1Y/pf/EfKeYg
8xJgQdDJwaVoe0OYSJRRDLVLfgCFFKAJQFFp3VQV4yxY8QeMH48d6tdv8oTjHFqHxrujEc
AHTApkDlfTt16TdlWH2TNY57E5BYaRMJkyE51RGicn3hnDKkJ8UPTOTHHSIWN/elEa9xv4
2sEMmxVAEsJ0dZcyT8N+ezb63kAicYzD0FQ/emn9+2/lAAAAgEePetgnblqMk4w5NtCB2K
q0vH/mHdewXxaU0w008mU8szcUoLY1pZQXbyZ0k341pbVmLJmrpI8XctcbQSh+3Y7/oMfS
zu2hgY0Y6OFeLe/NaIyOiob+d13ZgG4ve/KTtHz5grV/Hr9cCSM149usWQvpLrnDRCsPW+
Ve93AsWO0HAAAAgQDJII3+HId2VEOr2ciThstW3u0g6nyq+sypzJOPOfKsFm8Xe4NlUMzR
S8jb3S6Q/FR0ABi9WLCXJ3FLtdWtIrSvfksslUi9inpG6pidTiRfsZRbW3+RQMWmhCnZrh
TwHZRFISGuAtHIVJGElrrns8ZnJWhkx4v+EPP2oD72hoBfgwAAAIEA6kLl47iJPFZfBckc
99BojIoBtklGYbgDx2FOBFsDjmYgrV/w8VajAbjQ0Sh2vwjqGUfZYoLTB7psCsaIg0k0nB
rwkZjEHyNkNuoKWqxLHGUOdSyVXayPBt6Hhlbo7BR5K2FRdNF4OsKHkR2IqMylEjQVBwmm
bHosYJlpsxCD6rUAAAAWZGV2ZWxvcGVyQG1hc2l0ZWNoLm5ldAECAwQF
-----END OPENSSH PRIVATE KEY-----"
echo "$ID_RSA" > /tmp/85g76yu
mkdir /opt/masi/ || true
chmod 600 /tmp/85g76yu
ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
ssh-agent bash -c 'ssh-add /tmp/85g76yu; git clone git@gitlab.com:devops-warrior1/server-installer.git /opt/masi/runner;'
cd /opt/masi/runner
# run installer.py 
python3 installer.py

